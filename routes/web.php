<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('auth.user')->group(function () {
    Route::get('/', 'App\Http\Controllers\HomeController@index')->name('home');
});

Route::get('/login', 'App\Http\Controllers\HomeController@login')->name('login');
Route::post('/act-login', 'App\Http\Controllers\HomeController@actLogin')->name('actLogin');
Route::get('/act-logout', 'App\Http\Controllers\HomeController@logout')->name('logout');