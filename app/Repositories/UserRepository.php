<?php
namespace App\Repositories;
use Cookie;
use App\Models\User;

class UserRepository
{   
	public function logged(){
		$dataReturn['logged'] = 0;
		$dataReturn['info']   = [];
		$userInfo = Cookie::get("userInfo");
		if($userInfo != null && $userInfo != ""){
			$userInfo = json_decode($userInfo,1);
			if(count($userInfo) > 0){
				$email = replaceMQ($userInfo['email']);
				$password = replaceMQ($userInfo['password']);
				$dataUser = User::where('email',$email)->where('password',md5($password))->first();
				if($dataUser){
					$dataReturn['logged'] = 1;
					$dataReturn['info'] = $dataUser->toArray();
				}
			}
		}
		return $dataReturn;
	}

	public function setLogout(){
		Cookie::queue(Cookie::forget("userInfo"));
	}

	public function login($data)
	{
		$email = replaceMQ($data['email']);
		$password = replaceMQ($data['password']);
		// User
		$dataUser = User::where('email',$email)->where('password',md5($password))->first();
		return $dataUser;
	}
}
