<?php 
namespace App\Helper;

use File;
use Intervention\Image\Facades\Image;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ImageManager
{
	const USER_ADMIN_IMAGE_PATH  = 'http: //localhost: 5039/data/product/';
	const USER_ADMIN_SMALL_SIZE  = '85x85';
	const USER_ADMIN_MEDIUM_SIZE = '300x366';

	const CATEGORY_IMAGE_PATH    = 'uploads/category/';
	const TYPE_IMAGE_PATH        = 'uploads/types/';
	const ROOMTYPE_IMAGE_PATH    = 'uploads/roomtypes/'; 
	const HOTEL_IMAGE_PATH 		  = 'uploads/hotel/';

	public static function upload($file, $type, $thumbnail = true)
	{
		//extension
		$ext = $file->getClientOriginalExtension();

		//random 16 characters
		$filename = md5(str_random());

		$folderPath = self::getContainerFolder($type, $filename);

		//get and create container folder if needed
		if (!is_dir(public_path($folderPath)))
		{
			File::makeDirectory(public_path($folderPath), 0777, true);
		}

		//full path
		$path = public_path($folderPath . '/' . $filename . '.' . $ext);

		//save image to path
		Image::make($file->getRealPath())->save($path);

		//create and save thumbnails
		if ($thumbnail)
		{
			$thumbSize = [];

			/** USER_ADMIN */
			if ($type === 'user_admin')
			{
				$thumbSize[] = self::USER_ADMIN_SMALL_SIZE;
				$thumbSize[] = self::USER_ADMIN_MEDIUM_SIZE;
			}
			/** Brand */
			elseif ($type === 'category')
			{
				
			}
			elseif ($type === 'types')
			{
				
			}
			elseif ( $type === 'roomtype') {
				$thumbSize[] = self::USER_ADMIN_SMALL_SIZE;
				$thumbSize[] = self::USER_ADMIN_MEDIUM_SIZE;
			}
		
			if (!empty($thumbSize))
			{
				foreach ($thumbSize as $thumb)
				{
					$pathThumb = public_path($folderPath . '/' . $filename . '_' . $thumb . '.' . $ext);
					self::createThumb($pathThumb, $file, $thumb);
				}
			}
		}

		return $filename . '.' . $ext;
	}

	public static function getThumb($filename, $type, $size = 'small')
	{
		$thumb = '';

		$folder = self::getContainerFolder($type, $filename);

		/** USER_ADMIN */
		if ($type === 'user_admin')
		{
			if ($size === 'small') $thumbSize = self::USER_ADMIN_SMALL_SIZE;
			if ($size === 'medium') $thumbSize = self::USER_ADMIN_MEDIUM_SIZE;
		}
		/** Brand */
		elseif ($type === 'category')
		{
			
		}
		elseif ($type === 'types')
		{
			
		}
		elseif ( $type === 'roomtype') {
				if ($size === 'small') $thumbSize = self::USER_ADMIN_SMALL_SIZE;
			if ($size === 'medium') $thumbSize = self::USER_ADMIN_MEDIUM_SIZE;
		}
		
		if (strpos($filename, '.'))
		{
			$temp = explode('.', $filename);
			$thumb = $temp[0] . '_' . $thumbSize . '.' . $temp[1];
		}

		return $folder . $thumb;
	}

	public static function getContainerFolder($type, $filename = '')
	{
		if ($type === 'user_admin' && !$filename) {
			return null;
		}

		if ($type === 'user_admin') {
			return self::USER_ADMIN_IMAGE_PATH;
		}elseif ($type === 'category') {
			return self::CATEGORY_IMAGE_PATH;
		}elseif ($type === 'types') {
			return self::TYPE_IMAGE_PATH;
		}elseif ($type === 'roomtype') {
			return self::ROOMTYPE_IMAGE_PATH;
		}elseif ($type === 'hotel') {
			return self::HOTEL_IMAGE_PATH;
		}
		

		return null;
	}

	private static function createThumb($path, UploadedFile $file, $thumbSize = self::USER_ADMIN_SMALL_SIZE)
	{
		$sizeThumb = explode('x', $thumbSize);

		return self::iResize($file, $sizeThumb[0], $sizeThumb[1])->save($path);
	}

	private static function iResize(UploadedFile $file, $newW, $newH)
	{
		list($width, $height) = getimagesize($file->getRealPath());

		//if old width is bigger than old height, and new width is smaller than new height
		if ( ($width > $height && $newW < $newH) || ($width < $height && $newW > $newH) )
		{
			$temp = $newH;
			$newH = $newW;
			$newW = $temp;
		}

		//resize first
		$widthRatio = $width / $newW;
		$heightRatio = $height / $newH;

		if ($widthRatio > $heightRatio)
		{
			$resized = Image::make($file->getRealPath())->resize($newW, null, function($constraint)
			{
				$constraint->aspectRatio();
			});
		} else
		{
			$resized = Image::make($file->getRealPath())->resize(null, $newH, function($constraint)
			{
				$constraint->aspectRatio();
			});
		}

		//crop after resizing
		//return $resized->crop($newW, $newH);

		return $resized;
	}

}
