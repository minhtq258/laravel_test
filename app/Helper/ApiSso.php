<?php
namespace App\Helper;

use Request;
use Cookie;

class ApiSso
{
	public static $timeOutConnect = 30000;
	public static $hostDB         = 'https://apiws.8trip.vn';
	// info get token
	public static $AgencyId       = 200;
	public static $AgencyName     = "travellink";
	public static $appCode        = "web";
	public static $appVersion     = "1.0";
	public static $miliscond      = 1698342762544;
	public static $appVersionKey  = "nurbtvUBY857485yBUYB";

	public function __construct(){}

	public static function getAppToken(){
		$app_token = "";
		$info = [ 	'id' => self::$AgencyId, 
					'n' => self::$AgencyName, 
					'c' => self::$appCode, 
					'v' => self::$appVersion, 
					'dt' => self::$miliscond, 
				];
		$sign = md5(json_encode($info).self::$appVersionKey);
		$plain = json_encode(['info' => $info, 'sign' => $sign]);
		if($sign != "" && $plain != ""){
			$app_token = base64_encode($plain);
		}
		return $app_token;
	}

	/**
    * [curlGet Function getĐata]
    * @param  [type]  $url        [description]
    * @param  [type]  $options    [description]
    * @param  integer $add_log    [description]
    * @param  string  $prefix_log [description]
    * @return [type]              [description]
    */
	public static function curlGet($url, $type = 0, $add_log = 0, $prefix_log = ''){

		$dataReturn                 = [];
		$dataReturn['errorMessage'] = [];
		$dataReturn['data']         = [];
		$dataReturn['status']       = [];
		$AppToken                   = self::getAppToken();
		
		$userToken = "";
		$header = ['content-type: application/json'];
		if(Cookie::get("userLogin") !== ""){
            $userLogin = json_decode(Cookie::get("userLogin"),1);
            $userToken = $userLogin['token'];
		}

		if($AppToken != ""){
			if($type == 1){
				$header = ['content-type: application/x-www-form-urlencoded', 'app-token:'. $AppToken, 'user-token:'. $userToken];
			}else{
				$header = ['content-type: application/json', 'app-token:'. $AppToken, 'user-token:'. $userToken];
			}
		}

		$curl = curl_init();
		curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $curl, CURLOPT_SSL_VERIFYPEER, false );
		//thoi gian mac dinh cho phep connect
		curl_setopt( $curl, CURLOPT_TIMEOUT, self::$timeOutConnect);
		curl_setopt( $curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt( $curl, CURLOPT_HTTPHEADER, $header);
		curl_setopt( $curl, CURLOPT_CUSTOMREQUEST, 'GET' );
		curl_setopt( $curl, CURLOPT_URL, $url );
		$data            = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
		$data            = array_shift($data);
		
		$result_response = curl_exec( $curl );
		$error           = curl_error( $curl );
		$code            = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		$url             = curl_getinfo($curl, CURLINFO_EFFECTIVE_URL );
		
		$dataLog         = array();
		$dataLog['data'] = $result_response;
		$dataLog["code"] = $code;
		$dataLog["url"]  = $url;

		// Ghi log nếu có lỗi (nếu cần)
		if($add_log == 1){}  
		$result = json_decode($result_response,true);

		if(isset($result['status']) && $result['status'] != 0){
			$dataReturn['errorMessage'] = isset($result['error']) ? $result['error'] : 'Error';
		}else{
			$dataReturn['data'] = $result['data'];
		}
		$dataReturn['status'] = $result['status'];
		return $dataReturn;
	}

	public static function curlPost($url, $data_post = [], $type = 0, $add_log = 0, $prefix_log = ''){
		$dataReturn                 = [];
		$dataReturn['errorMessage'] = [];
		$dataReturn['data']         = [];
		$dataReturn['status']       = [];
		$AppToken                   = self::getAppToken();
		
		$userToken = "";
		$header = ['content-type: application/json'];
		if(Cookie::get("userLogin") !== ""){
            $userLogin = json_decode(Cookie::get("userLogin"),1);
            $userToken = $userLogin['token'];
		}

		if($AppToken != ""){
			if($type == 1){
				$header = ['Cache-Control: no-cache','content-type: application/x-www-form-urlencoded', 'app-token:'. $AppToken, 'user-token:'. $userToken];
			}else{
				$header = ['Cache-Control: no-cache','content-type: application/json', 'app-token:'. $AppToken, 'user-token:'. $userToken];
			}
		}

		$curl = curl_init();
		curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $curl, CURLOPT_SSL_VERIFYPEER, false );
		//thoi gian mac dinh cho phep connect
		curl_setopt( $curl, CURLOPT_TIMEOUT, self::$timeOutConnect);
		curl_setopt( $curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt( $curl, CURLOPT_HTTPHEADER, $header);
		curl_setopt( $curl, CURLOPT_POST, 1);
		curl_setopt( $curl, CURLOPT_URL, $url );
		curl_setopt( $curl, CURLOPT_FRESH_CONNECT, TRUE);
		curl_setopt( $curl, CURLOPT_ENCODING, 'gzip'); 
		if(count($data_post) > 0){
			if($type == 1){
				curl_setopt( $curl, CURLOPT_POSTFIELDS, http_build_query($data_post));
				// curl_setopt( $curl, CURLOPT_POSTFIELDS, $data_post);
			}else{
				curl_setopt( $curl, CURLOPT_POSTFIELDS, json_encode($data_post));
			}
			
		}

		$result_response = curl_exec( $curl );
		$error           = curl_error( $curl );
		$code            = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		$url             = curl_getinfo($curl, CURLINFO_EFFECTIVE_URL );
		$dataLog              = array();
		$dataLog['data_post'] = json_encode($data_post);
		$dataLog['response']  = $result_response;
		$dataLog["code"]      = $code;
		$dataLog["url"]       = $url;
		// Ghi log nếu có lỗi (nếu cần)
		if($add_log == 1){}
	
		$result = json_decode($result_response,true);

		if(isset($result['status']) && $result['status'] != 0){
			$dataReturn['errorMessage'] = isset($result['error']) ? $result['error'] : 'Error';
		}else{
			$dataReturn['data'] = $result['data'];
		}

		$dataReturn['status'] = $result['status'];
		return $dataReturn;
	} 

	public static function curlGetHotel($url,$type = 0, $add_log = 0, $prefix_log = ''){

		$dataReturn                 = [];
		$dataReturn['errorMessage'] = [];
		$dataReturn['data']         = [];
		$dataReturn['status']       = [];
		$AppToken                   = self::getAppToken();
		$userToken = "";
		$header = ['content-type: application/json'];
		if(Cookie::get("userLogin") !== ""){
            $userLogin = json_decode(Cookie::get("userLogin"),1);
            $userToken = $userLogin['token'];
		}

		if($AppToken != ""){
			if($type == 1){
				$header = ['Cache-Control: no-cache','content-type: application/x-www-form-urlencoded', 'app-token:'. $AppToken, 'user-token:'. $userToken];
			}else{
				$header = ['Cache-Control: no-cache','content-type: application/json', 'app-token:'. $AppToken, 'user-token:'. $userToken];
			}
		}

		$curl = curl_init();
		curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $curl, CURLOPT_SSL_VERIFYPEER, false );
		//thoi gian mac dinh cho phep connect
		curl_setopt( $curl, CURLOPT_TIMEOUT, 30);
		curl_setopt( $curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt( $curl, CURLOPT_HTTPHEADER, $header);
		curl_setopt( $curl, CURLOPT_FRESH_CONNECT, TRUE);
		curl_setopt( $curl, CURLOPT_ENCODING , "gzip");
		curl_setopt( $curl, CURLOPT_CUSTOMREQUEST, 'GET' );
		curl_setopt( $curl, CURLOPT_URL, $url );
		$data            = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
		$data            = array_shift($data);
		
		$result_response = curl_exec($curl);
		$error           = curl_error($curl);
		$code            = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		$url             = curl_getinfo($curl, CURLINFO_EFFECTIVE_URL );
		
		$dataLog         = array();
		$dataLog['data'] = $result_response;
		$dataLog["code"] = $code;
		$dataLog["url"]  = $url;

		// Ghi log nếu có lỗi (nếu cần)
		if($add_log == 1){}  
		$result = json_decode($result_response,true);

		if(isset($result['status']) && $result['status'] != 0){
			$dataReturn['errorMessage'] = isset($result['error']) ? $result['error'] : 'Error';
		}else{
			$dataReturn['data'] = $result['result'];
		}
		$dataReturn['status'] = $result['status'];
		return $dataReturn;
	}

	public static function curlPostHotel($url, $data_post = [],$type= 0, $add_log = 0, $prefix_log = ''){
		$dataReturn                 = [];
		$dataReturn['errorMessage'] = [];
		$dataReturn['data']         = [];
		$dataReturn['status']       = [];

		$AppToken                   = self::getAppToken();
		$userToken = "";
		$header = ['content-type: application/json'];
		if(Cookie::get("userLogin") !== ""){
            $userLogin = json_decode(Cookie::get("userLogin"),1);
            $userToken = $userLogin['token'];
		}

		if($AppToken != ""){
			if($type == 1){
				$header = ['content-type: application/x-www-form-urlencoded', 'app-token:'. $AppToken, 'user-token:'. $userToken];
			}else{
				$header = ['content-type: application/json', 'app-token:'. $AppToken, 'user-token:'. $userToken];
			}
		}

		$curl = curl_init();
		curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $curl, CURLOPT_SSL_VERIFYPEER, false );
		//thoi gian mac dinh cho phep connect
		curl_setopt( $curl, CURLOPT_TIMEOUT, self::$timeOutConnect);
		curl_setopt( $curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt( $curl, CURLOPT_HTTPHEADER, $header);
		curl_setopt( $curl, CURLOPT_POST, 1);
		curl_setopt( $curl, CURLOPT_URL, $url );
		curl_setopt( $curl, CURLOPT_ENCODING, 'gzip'); 
		if($data_post){
			// curl_setopt( $curl, CURLOPT_POSTFIELDS, http_build_query($data_post));
			curl_setopt( $curl, CURLOPT_POSTFIELDS, json_encode($data_post));
		}

		$result_response = curl_exec( $curl );
		$error           = curl_error( $curl );
		$code            = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		$url             = curl_getinfo($curl, CURLINFO_EFFECTIVE_URL );

		$dataLog              = array();
		$dataLog['data_post'] = json_encode($data_post);
		$dataLog['response']  = $result_response;
		$dataLog["code"]      = $code;
		$dataLog["url"]       = $url;

		// Ghi log nếu có lỗi (nếu cần)
		if($add_log == 1){}
			
		$result = json_decode($result_response,true);

		if(isset($result['status']) && $result['status'] != 0){
			$dataReturn['errorMessage'] = isset($result['error']) ? $result['error'] : 'Error';
		}else{
			$dataReturn['data'] = $result['result'];
		}

		$dataReturn['status'] = $result['status'];
		return $dataReturn;
	} 

	public static function curlPostHotel2($url, $data_post = [], $type = 0,$add_log = 0, $prefix_log = ''){
		$dataReturn                 = [];
		$dataReturn['errorMessage'] = [];
		$dataReturn['data']         = [];
		$dataReturn['status']       = [];

		$AppToken                   = self::getAppToken();
		$userToken = "";
		$header = ['content-type: application/json'];
		if(Cookie::get("userLogin") !== ""){
            $userLogin = json_decode(Cookie::get("userLogin"),1);
            $userToken = $userLogin['token'];
		}

		if($AppToken != ""){
			if($type == 1){
				$header = ['content-type: application/x-www-form-urlencoded', 'app-token:'. $AppToken, 'user-token:'. $userToken];
			}else{
				$header = ['content-type: application/json', 'app-token:'. $AppToken, 'user-token:'. $userToken];
			}
		}

		$curl = curl_init();
		curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $curl, CURLOPT_SSL_VERIFYPEER, false );
		//thoi gian mac dinh cho phep connect
		curl_setopt( $curl, CURLOPT_TIMEOUT, self::$timeOutConnect);
		curl_setopt( $curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt( $curl, CURLOPT_HTTPHEADER, $header);
		curl_setopt( $curl, CURLOPT_POST, 1);
		curl_setopt( $curl, CURLOPT_URL, $url );
		curl_setopt( $curl, CURLOPT_ENCODING, 'gzip'); 
		if($data_post){
			// curl_setopt( $curl, CURLOPT_POSTFIELDS, http_build_query($data_post));
			curl_setopt( $curl, CURLOPT_POSTFIELDS, json_encode($data_post));
		}

		$result_response = curl_exec( $curl );
		$error           = curl_error( $curl );
		$code            = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		$url             = curl_getinfo($curl, CURLINFO_EFFECTIVE_URL );

		$dataLog              = array();
		$dataLog['data_post'] = json_encode($data_post);
		$dataLog['response']  = $result_response;
		$dataLog["code"]      = $code;
		$dataLog["url"]       = $url;

		// Ghi log nếu có lỗi (nếu cần)
		if($add_log == 1){}
			
		$result = json_decode($result_response,true);

		$dataReturn = $result;
		return $dataReturn;
	}
}