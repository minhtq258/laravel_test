<?php

namespace App\Http\Middleware;
use Closure;
use Route;
use Lang;
use Illuminate\Http\Request;
use Cookie;
use App\Repositories\UserRepository;

class UserAuthenticate
{
    public $UserRepository;
    public function __construct(UserRepository $UserRepository){
        $this->UserRepository = $UserRepository;
    }

    // check đăng nhập user
    public function handle($request, Closure $next){

        $returnLogged    = $this->UserRepository->logged();
        if($returnLogged['logged'] <= 0){
            return redirect()->route('login')->with('error','Bạn không có quyền xem thông tin này!');
        }else{
            app()->singleton('USER_INFO', function() use ($returnLogged){
                return $returninfo['info'];
            });
        }

        return $next($request);
    }
}
