<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\UserRepository;
use Cookie;

class HomeController extends FrontEndController
{

    public function __construct(UserRepository $UserRepository){
        $this->UserRepository = $UserRepository;
    }

    public function index(Request $request){ 
        $data = [];
        return view('home')->with($data);
    }

    public function login(Request $request){
        $data = [];
        return view('login.form')->with($data);
    }

    public function actLogin(Request $request){

        $this->validate($request, [
            'email' => 'required',
            'password' => 'required',
        ]);
        
        $username = replaceMQ($request->get('email'));
        $password = replaceMQ($request->get('password'));

        if($username != "" && $password != ""){
            $info = $this->UserRepository->login(['email' => $username,'password' => $password]);
            if($info){
                Cookie::queue("userInfo", json_encode(['email' => $username,'password' => $password]), 60*24*60*7);
                return redirect()->route('home');
            }else{
                return redirect()->route('login')->with('error','Thông tin tài khoản không chính xác!');
            }
        }else{
            return redirect()->route('login')->with('error','Thông tin tài khoản không chính xác!');
        }
    }

    public function logout(){
        $this->UserRepository->setLogout();
        return redirect()->route('login')->with('success','Đăng xuất thành công!');
    }
}
