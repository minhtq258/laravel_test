<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js" type="text/javascript"></script>
        <script src="{{asset('jquery.growl.js')}}" type="text/javascript"></script>
        <link rel="stylesheet" type="text/css" href="{{asset('jquery.growl.css')}}">
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    
        <style>
            body {
                font-family: 'Nunito', sans-serif;
            }
        </style>
    </head>
    <body class="antialiased" style="margin:20px;">
        <h3>{{ app()->make('App\DemoMinh')->fullname() }}</h3> <a href="{{ route('logout') }}">Logout</a>
    </body>
    @if (Session::has('success'))
      <script type="text/javascript"> $.growl.notice({message: "{{ Session::get('success') }}"}); </script>
    @endif
    @if (Session::has('error'))
      <script type="text/javascript"> $.growl.error({message: "{{ Session::get('error') }}"}); </script>
    @endif
    @if (Session::has('warning'))
      <script type="text/javascript"> $.growl.warning({message: "{{ Session::get('warning') }}"}); </script>
    @endif
    </body>
</html>
