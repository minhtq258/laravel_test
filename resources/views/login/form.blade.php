<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Login</title>
        <!-- Fonts -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js" type="text/javascript"></script>
        <script src="{{asset('jquery.growl.js')}}" type="text/javascript"></script>
        <link rel="stylesheet" type="text/css" href="{{asset('jquery.growl.css')}}">
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    </head>
    <body>
        
        <div class="row justify-content-md-center">
            
            <form method="POST" action="{{route('actLogin')}}" class="col-lg-4 mt-3 ml-3 justify-content-center">
                @if ($errors->any())
                    <div style="color: #ff0000;">
                        <ul>
                            <li>{{ $errors->first() }}</li>
                        </ul>
                    </div>
                @endif
                @csrf
                <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">Email address</label>
                    <input type="email" class="form-control" name="email" id="exampleInputEmail1" aria-describedby="emailHelp">
                    <div id="emailHelp" class="form-text"></div>
                </div>
                <div class="mb-3">
                    <label for="exampleInputPassword1" class="form-label">Password</label>
                    <input type="password" class="form-control" name="password" id="exampleInputPassword1">
                </div>
                 <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
        @if (Session::has('success'))
          <script type="text/javascript"> $.growl.notice({message: "{{ Session::get('success') }}"}); </script>
        @endif
        @if (Session::has('error'))
          <script type="text/javascript"> $.growl.error({message: "{{ Session::get('error') }}"}); </script>
        @endif
        @if (Session::has('warning'))
          <script type="text/javascript"> $.growl.warning({message: "{{ Session::get('warning') }}"}); </script>
        @endif
    </body>
</html>
